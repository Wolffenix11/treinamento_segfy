Rails.application.routes.draw do
  resources :veiculos, only: [:index, :show] do
    collection do
      get '/valores'                    => 'veiculos#search_by_price'
      get '/modelo/:modelo_id'          => 'veiculos#search_by_model'
      get '/modelo/:modelo_id/ano/:ano' => 'veiculos#search_by_year'
    end
  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  
  resources :marcas, only: [:index, :show] do
    collection do
      get '/tipo/:tipo'           => 'marcas#search_by_type'
      get '/descricao/:descricao' => 'marcas#search_by_brand'
    end
  end

  resources :modelos, only: [:index, :show] do
    collection do
      get '/marca/:marca_id' => 'modelos#search_by_brand'
      get '/descricao/:descricao'    => 'modelos#search_by_model'
      get '/marca/:marca_id/ano/:ano' => 'modelos#search_by_year'
    end
  end

  mount Sidekiq:: Web, at: '/sidekiq'
end
