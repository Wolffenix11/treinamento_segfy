require 'sidekiq/web'
url_redis = "redis://127.0.0.1:6379"
Sidekiq.configure_server do |config|
    config.redis = {:url => url_redis }
end
Sidekiq.configure_client do |config|
    config.redis = {:url => url_redis }
end
Sidekiq::Web.use(Rack::Auth::Basic) do |user, password|
    [user, password] == ['admin', 'senha']
end