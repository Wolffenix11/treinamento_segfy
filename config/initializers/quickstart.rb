require 'rufus-scheduler'

scheduler = Rufus::Scheduler.new(:lockfile => ".rufus-scheduler.lock")
scheduler.cron '56 11 * * *' do
    WorkerMarca.perform_async(1)
end if scheduler.lock

scheduler.cron '57 11 * * *' do
    WorkerMarca.perform_async(2)
end if scheduler.lock

scheduler.cron '58 11 * * *' do
    WorkerMarca.perform_async(3)
end if scheduler.lock