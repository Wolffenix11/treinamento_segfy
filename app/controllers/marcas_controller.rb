class MarcasController < ApplicationController
  before_action :set_marca, only: %i[ show ]

  # GET /marcas or /marcas.json
  def index
    @marcas = Marca.all
    render json: @marcas
  end

  def search_by_type
    @marcas = Marca.where(tipo: params[:tipo])
    render json: @marcas
  end

  def search_by_brand
    @marcas = Marca.where("marcas.descricao ilike ?", "#{params[:descricao]}%")
    render json: @marcas
  end
  
  

  # GET /marcas/1 or /marcas/1.json
  def show
    render json: @marca
  end

  private
    # Use callbacks to share common setup or constraints between actions.
  def set_marca
    @marca = Marca.find(params[:id])
  end
end
