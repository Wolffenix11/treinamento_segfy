class VeiculosController < ApplicationController
  before_action :set_veiculo, only: %i[ show ]

  # GET /veiculos or /veiculos.json
  def index
    @veiculos = Veiculo.all.paginate(page: params[:page], per_page: 30)
    render json: @veiculos
  end

  # GET /veiculos/1 or /veiculos/1.json
  def show
    render json: @veiculo
  end

  def search_by_model
    @veiculos = Veiculo.where(modelo_id: params[:modelo_id])
    render json: @veiculos
  end

  def search_by_year
    @veiculos = Veiculo.where(modelo_id: params[:modelo_id], ano: params[:ano])
    render json: @veiculos
  end

  def search_by_price
    @veiculos = Veiculo.joins(modelo: :marca).where(marca: {tipo: params[:tipo]}, preco: (params[:start_value].to_f..params[:end_value].to_f))
    render json: @veiculos, each_serializer: VeiculoSerializer
  end
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_veiculo
      @veiculo = Veiculo.find(params[:id])
    end
    
end
