class ApplicationController < ActionController::API
	
    include ActionController::HttpAuthentication::Basic::ControllerMethods
	before_action :authentication

	attr_accessor :current_user

    def authentication
        if user = authenticate_with_http_basic { |u, p| User.find_by(login: u, password: p)}
            @current_user = user
        else
            request_http_basic_authentication
        end
    end

end