class ModelosController < ApplicationController
  before_action :set_modelo, only: %i[ show ]

  # GET /modelos or /modelos.json
  def index
    @modelos = Modelo.all.paginate(page: params[:page], per_page: 30)
    render json: @modelos
  end

  # GET /modelos/1 or /modelos/1.json
  def show
    render json: @modelo
  end

  def search_by_brand
    @modelos = Modelo.where(marca_id: params[:marca_id])
    render json: @modelos
  end
  
  def search_by_model
    @modelos = Modelo.where("modelos.descricao ilike ?", "#{params[:descricao]}%")
    render json: @modelos
  end

  def search_by_year
    @modelos = Modelo.joins(:veiculos).where(marca_id: params[:marca_id], veiculos: {ano: params[:ano]})
    render json: @modelos
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_modelo
      @modelo = Modelo.find(params[:id])
    end
end
