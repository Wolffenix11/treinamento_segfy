class WorkerModelo
    
    include Sidekiq::Worker
    
    sidekiq_options queue: :modelo, :retry => false

    def perform(type, reference, brand_id, codigo_marca)
        ModelosUtils.save_models(type, reference, brand_id, codigo_marca)
        
        Modelo.where(marca_id: brand_id).all.each do |modelo|
            WorkerVeiculo.perform_async(type, reference, modelo.id, codigo_marca, modelo.codigo)
        end

    rescue => exception
        puts '===========V==========='
        puts '-----Error Modelo-----'
        puts "Tipo da busca #{type}"
        puts "Referencia da busca #{reference}"
        puts "Marca da busca #{brand_id}"
        puts "Codigo da marca #{codigo_marca}"
        puts exception.class
        puts exception.to_s
        puts exception.backtrace.to_s
    end

end