class WorkerMarca
    include Sidekiq::Worker
    
    sidekiq_options queue: :marca, :retry => false

    def perform(type)
        MarcasUtils.save_brands(type)

        reference = MarcasUtils.get_month_reference

        Marca.where(tipo: type).all.each do |marca|
            WorkerModelo.perform_async(type, reference, marca.id, marca.codigo)
        end

    rescue => exception 
        puts '===========V==========='
        puts '-----Error Marca-----'
        puts "Tipo da busca #{type}"
        puts exception.class
        puts exception.to_s
        puts exception.backtrace.to_s
    end

end
