class WorkerVeiculo
    
    include Sidekiq::Worker
    
    sidekiq_options queue: :veiculo, :retry => false

    def perform(type, reference, model_id, codigo_marca, codigo_modelo)
        VehicleUtils.save_vehicles(type, reference, model_id, codigo_marca, codigo_modelo)    
    
    rescue => exception
        puts '===========V==========='
        puts '-----Error Modelo-----'
        puts "Tipo da busca #{type}"
        puts "Referencia da busca #{reference}"
        puts "Modelo da busca #{model_id}"
        puts "Codigo da marca #{codigo_marca}"
        puts "Codigo do modelo #{codigo_modelo}"
        puts exception.class
        puts exception.to_s
        puts exception.backtrace.to_s
    end

end