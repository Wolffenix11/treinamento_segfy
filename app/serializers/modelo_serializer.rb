class ModeloSerializer < ActiveModel::Serializer
    attributes :id, :tipo, :marca, :descricao, :anos_modelos

    def tipo
        object.marca.tipo
    end

    def marca
        object.marca.descricao
    end

    def anos_modelos
        object.veiculos ? object.veiculos.map{|veiculo| veiculo.ano} : []
    end

end