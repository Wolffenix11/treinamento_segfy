class VeiculoSerializer < ActiveModel::Serializer
    attributes :id, :tipo, :ano, :cod_fipe, :combustivel, :marca, :modelo, :preco

    def tipo
        object.modelo.marca.tipo
    end

    def marca
        object.modelo.marca.descricao
    end

    def modelo
        object.modelo.descricao 
    end

    def preco
        object.preco.reais.to_s
    end
end