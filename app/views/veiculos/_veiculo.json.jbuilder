json.extract! veiculo, :id, :modelo_id, :ano, :cod_fipe, :preco, :combustivel, :created_at, :updated_at
json.url veiculo_url(veiculo, format: :json)
