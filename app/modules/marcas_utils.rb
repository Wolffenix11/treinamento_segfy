require 'mechanize'

module MarcasUtils
    
    extend MechanizeUtils
    module_function

    def get_brands(type)
        url = 'https://veiculos.fipe.org.br/api/veiculos//ConsultarMarcas'

        options = {
            'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'
        }

        params = {
            'codigoTabelaReferencia' => get_month_reference,
            'codigoTipoVeiculo'      => type
        }
        brands = mechanize.post(url, params, options)

        brands = JSON.parse(brands.body)
    end

    def get_month_reference
        url = 'https://veiculos.fipe.org.br/api/veiculos//ConsultarTabelaDeReferencia'         
        months = mechanize.post(url)

        months_reference = JSON.parse(months.body)

        months_reference.map {|cod| cod['Codigo']}.max
    end

    def save_brands(type)
        brands = get_brands(type)

        brands.each do |brand|
            Marca.find_or_create_by({
                descricao: brand['Label'],
                codigo: brand['Value'], 
                tipo: type
            })
        end
    end

end


