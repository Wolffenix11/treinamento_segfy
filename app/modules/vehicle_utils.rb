require 'brdinheiro'

module VehicleUtils
    
    extend MechanizeUtils
    module_function

    VEHICLE_TYPE = {
        '1' => 'carro',
        '2' => 'moto',
        '3' => 'caminhao'
    }

    def get_vehicles(type, reference, codigo_marca, codigo_modelo)
        years = get_model_year(type, reference, codigo_marca, codigo_modelo)
        url = 'https://veiculos.fipe.org.br/api/veiculos//ConsultarValorComTodosParametros'

        options = {
            'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'
        }

        years.map do |year|
        
            params = {
                'codigoTabelaReferencia' => reference,
                'codigoMarca'            => codigo_marca,
                'codigoModelo'           => codigo_modelo,
                'codigoTipoVeiculo'      => type,
                'anoModelo'              => year.split('-').first,
                'codigoTipoCombustivel'  => year.split('-').last,
                'tipoVeiculo'            => VEHICLE_TYPE[type.to_s],
                'modeloCodigoExterno'    => '',
                'tipoConsulta'           => 'tradicional',
            }
            vehicles = mechanize.post(url, params, options)
            vehicles = JSON.parse(vehicles.body)
        end
    end

    def save_vehicles(type, reference, model_id, codigo_marca, codigo_modelo)
        vehicles = get_vehicles(type, reference, codigo_marca, codigo_modelo)

        vehicles.each do |vehicle|
            ano_modelo = vehicle['AnoModelo'].eql?('32000') ? (Date.today.year + 1).to_s : vehicle['AnoModelo']
            
            vehicle_model = Veiculo.find_or_create_by ({
                ano: ano_modelo,
                combustivel: vehicle['SiglaCombustivel'], 
                cod_fipe: vehicle['CodigoFipe'], 
                modelo_id: model_id
            })
            vehicle_model.preco = vehicle['Valor'].real.to_f
            vehicle_model.save
        end

    end

    def get_model_year(type, reference, codigo_marca, codigo_modelo)
        url = 'https://veiculos.fipe.org.br/api/veiculos//ConsultarAnoModelo'

        params = {
            'codigoTipoVeiculo'      => type,
            'codigoTabelaReferencia' => reference,
            'codigoModelo'           => codigo_modelo,
            'codigoMarca'            => codigo_marca,
            'ano'                    => '',
            'codigoTipoCombustivel'  => '',
            'anoModelo'              => '',
            'modeloCodigoExterno'    => ''
        }

        years = mechanize.post(url, params)
        years = JSON.parse(years.body)
        years.map {|year| year['Value']}
    end
    
    
    
    
end