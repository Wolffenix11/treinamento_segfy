require 'mechanize'

module ModelosUtils
    
    extend MechanizeUtils
    module_function

    def get_models(type, reference, codigo)
        
        url = 'https://veiculos.fipe.org.br/api/veiculos//ConsultarModelos'

        options = {
            'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'
        }
        
        params = {
            'codigoTipoVeiculo'     => type,
            'codigoTabelaReferencia'=> reference,
            'codigoModelo'          => '',
            'codigoMarca'           => codigo,
            'ano'                   => '',
            'codigoTipoCombustivel' => '',
            'anoModelo'             => '',
            'modeloCodigoExterno'   => ''
        }
        models = mechanize.post(url, params, options)
        models = JSON.parse(models.body)
        models['Modelos']   
    end

    def save_models(type, reference, brand_id, codigo)
        models = get_models(type, reference, codigo)

        models.each do |model|
            Modelo.find_or_create_by({
                descricao: model['Label'],
                marca_id: brand_id,
                codigo: model['Value']
            })
        end
    end
    
end