require 'mechanize'

module NotificationSlack
    module_function
    URL = 'https://slack.com/api/'

    TOKEN = 'xoxp-136409773250-139798847395-141671079984-fa1f28085b2e7d129f99286c0388b675'

    USERS = {
        "antonio.carrilho"   => "U022EN3697E",
        "cristian.klein"     => "U8BKTPG67",
        "marco.silva"        => "U01DAUFQU8H",
        "marlon.ricardo"     => "U8BN096PL",
        "slackbot"           => "USLACKBOT",
    }
    GROUPS =  {
        "w_tests"           => "G435QG2N5"
    }

    def send_message(destinatary = 'marlon.ricardo', text = "No text.", attachments = [])
        params = {
            'token'       => TOKEN,
            'channel'     => USERS[ destinatary ] || GROUPS[ destinatary ],
            'as_user'     => true,
            'text'        => mention( text ),
            'attachments' => attachments.to_json
        }

        Mechanize.new.post(URL + 'chat.postMessage', params)
    end

    def mention(text)
        USERS.each do |user, token| 
            text.gsub!(/@#{user}/i, "<@#{token}>")
        end
        text
    end
end
 