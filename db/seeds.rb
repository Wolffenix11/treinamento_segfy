# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#marca = Marca.create(descricao: "Honda", codigo: "20", tipo: "carro")
#modelo = Modelo.create(descricao: "Civic 2.0 EXL Aut", codigo: "5", marca: marca)
#Modelo.create(descricao: "Civic 2.0 EXL Aut", codigo: "9", marca: marca)
#Veiculo.create(ano: "2021", preco: "149000", combustivel: "G", cod_fipe: "13456-90", modelo: modelo)

User.create(login: 'toninho', password: 'toninho')
User.create(login: 'marquinho', password: 'senha')
