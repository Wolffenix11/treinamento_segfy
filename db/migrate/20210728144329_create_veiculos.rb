class CreateVeiculos < ActiveRecord::Migration[6.1]
  def change
    create_table :veiculos do |t|
      t.references :modelo, null: false, foreign_key: true
      t.integer :ano
      t.string :cod_fipe
      t.float :preco
      t.string :combustivel

      t.timestamps
    end
  end
end
