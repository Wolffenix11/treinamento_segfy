class CreateModelos < ActiveRecord::Migration[6.1]
  def change
    create_table :modelos do |t|
      t.references :marca, null: false, foreign_key: true
      t.string :descricao
      t.string :codigo

      t.timestamps
    end
  end
end
