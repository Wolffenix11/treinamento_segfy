class CreateMarcas < ActiveRecord::Migration[6.1]
  def change
    create_table :marcas do |t|
      t.string :descricao
      t.string :codigo
      t.string :tipo

      t.timestamps
    end
  end
end
